<<<
== Intégrez votre code en continu

Dans ce chapitre, nous allons utiliser *GitLab* afin d'implémenter les différentes étapes de l'intégration continue pour garantir que notre code fonctionne à tout moment. Vous allez commencer par implémenter les étapes de *compilation* et de *tests*.

=== Clonez le projet sur votre poste

Pour pouvoir commencer à travailler sur le projet, afin de récupérer le code source, nous allons dans un premier temps *cloner le repository Git*. Pour ce faire, retournez sur la *page d'accueil du projet* où se trouvent toutes les instructions nécessaires au clonage du projet.

Cliquez en haut à droite sur le bouton bleu *Clone*, et copiez la deuxième ligne de la pop-up *Clone with HTTPS*.

Prenez ensuite une console d'ordinateur, représentant votre poste de développeur, et clonez le repository en tapant la commande suivante :

[source,bash,linenums]
----
git clone https://gitlab.com/[votre-nom-d-utilisateur]/spring-petclinic-microservices.git
cd spring-petclinic-microservices
----

Une fois le clone fait, nous nous retrouvons avec un dossier vide où la branche master est associée à notre repository GitLab. Nous allons ajouter une branche upstream sur GitHub et puller les dernières modifications GitHub. Les commandes à taper sont les suivantes :

[source,bash,linenums]
----
git remote add upstream https://github.com/spring-petclinic/spring-petclinic-microservices.git
git pull upstream master
git push origin master
----

Normalement, si les commandes précédentes ont été exécutées, le dossier Git devrait contenir le code source du projet PetClinic, ainsi que toutes les modifications associées

.Votre projet GitLab une fois le code cloné
image::images/gitlab.png[width=100%]

=== Activez l'intégration continue sur votre projet avec GitLab

Nous allons maintenant ajouter un *pipeline d'intégration continue*, afin d'implémenter les différentes étapes que nous avons vues précédemment. Les étapes de ce pipeline seront lancées successivement, lors de chaque nouveau push du code sur le repo. Voici à quoi ressemblera le pipeline :

.Les étapes que nous allons mettre en place
image::images/ci-vide.png[width=100%]

Pour *activer l'intégration continue sur GitLab*, le plus simple est de cliquer sur le bouton Set up CI/CD sur la page d'accueil du projet. Cette commande va créer le fichier `gitlab-ci.yml` dans votre projet. C'est sur ce fichier que vous décrirez tout votre pipeline CI/CD avec la syntaxe YAML.

Et d'ajouter les lignes suivantes dans le fichier :

[source,yaml,linenums]
----
stages:
  - build
  - test

cache:
  paths:
    - .m2/repository
  key: "$CI_JOB_NAME"

build_job:
  stage: build
  script:
    - ./mvnw compile
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
      --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true
  image: openjdk:8-alpine

test_job:
  stage: test
  script:
    - ./mvnw test
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
      --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true
  image: openjdk:8-alpine
----

NOTE: Pas de panique, nous allons détailler le fichier ensemble ! 😁 

Ce fichier est la pierre angulaire de l'implémentation d'un pipeline dans GitLab. Ce fichier s'appelle `.gitlab-ci.yml`, et c'est ici que nous allons définir notre pipeline. Dans cet exemple, nous avons implémenté *deux étapes* :

* l'étape de compilation avec la tâche `build_job` ;
* l'étape des tests avec la tâche `test_job`.

.Découvrons le fichier bloc par bloc !

==== Définissez les étapes du pipeline

Dans un premier temps, je définis les étapes de mon pipeline avec le mot clé `stages`. Ce mot clé permet de définir l'ordre des étapes. Ici, la première étape va être le `build`, et ensuite les `tests`. 

Ça veut dire que les *tâches* (ou jobs en anglais) associées à l'étape `build` (ici `build_job`) vont s'exécuter en premier, puis les tâches de l'étape `test` (ici `test_job`).

==== Accélérez les étapes avec le cache

Le deuxième bloc, avec le mot clé `cache`, est ici utilisé pour accélérer toutes nos étapes. Effectivement, dans le cas d'une compilation Java avec Maven (notre cas), cette compilation récupère beaucoup de dépendances et de librairies externes. Ces librairies sont stockées dans le répertoire `.m2`.

Grâce à l'utilisation du mot clé `cache` et de la variable prédéfinie de GitLab `$CI_JOB_NAME`, ce répertoire est commun à tous les jobs du pipeline.

==== Définissez les jobs à effectuer

Ensuite, je déclare deux jobs, correspondant chacun à une des étapes de notre pipeline d'intégration continue. Dans ces deux jobs, nous voyons que nous avons trois différentes lignes. Découvrons à quoi ces lignes correspondent :

* `stage` : c'est le nom de l'étape qui va apparaître dans notre pipeline d'intégration continue. Cela correspond aussi au `stage` auquel sera exécuté le job ;
* `script` :  ce sont les lignes de script à lancer afin d'exécuter l'étape. Ici, nous lançons le script Maven suivant son lifecycle. Dans la partie `build`, nous lançons la compilation ; et dans la partie `test`, nous lançons les tests de l'application. D'autres options sont définies afin d'accélérer le temps de traitement de ces lignes. Le `script` va alors télécharger Maven, l’outil de compilation, toutes les dépendances de l’application, et lancer la compilation du projet ;
* `image` : c'est l'image Docker qui va être lancée par GitLab afin d'exécuter les lignes de script que nous avons définies. Ici, l'image `openjdk:8-alpine`, qui contient déjà Java 8, va être lancée afin de pouvoir compiler le projet. Une fois le fichier sauvegardé, le pipeline de build se lance, et vous devriez voir les différentes étapes se lancer (ici, l'étape de build et l'étape de test).

Lors de l'étape de test, le pipeline va exécuter les tests unitaires déjà présents au sein du projet. L'objectif de cette étape est de s'assurer de lancer les tests écrits par les développeurs. Si un seul de ces tests échoue, le pipeline s'arrête.

.Si les tests échouent, le rôle du développeur est alors de :
* soit corriger le test qu'il a écrit, car la fonctionnalité a évolué et le test ne correspond plus ;
* soit faire évoluer le code, car le test a détecté un bug.

=== Lancez votre pipeline CI/CD

Pour voir le pipeline complet, il suffit de cliquer sur le sous-menu Pipelines dans le menu CI/CD.

.La page Pipeline avec toutes les exécutions du pipeline
image::images/pipeline.png[width=100%]

En cliquant sur le statut running du pipeline, nous avons plus de détails sur ce pipeline, les jobs associés ainsi que leurs statuts.

.Détail d'exécution du pipeline
image::images/pipeline-detail.png[width=100%]

=== Et si l'application contient un bug ?
==== Lancez votre pipeline avec un bug
NOTE: Afin de démontrer un workflow typique de CI/CD, nous allons introduire un bug dans l'application. 🐛 Ainsi, nous verrons comment le pipeline de CI/CD le détecte, et le corrige. Dans un premier temps, nous allons récupérer le fichier que nous venons de créer :

[source,bash,linenums]
----
git pull
----

Le dossier courant devrait avoir le nouveau fichier .gitlab-ci.yml. Nous allons éditer un fichier afin d'introduire un bug. Dans un premier temps, créez une nouvelle branche qui va accueillir nos modifications :

[source,bash,linenums]
----
git checkout -b refactor-customers
----

Ensuite, *éditez le fichier* Java "_spring-petclinic-customers-service/src/main/java/org/springframework/samples/petclinic/customers/CustomersServiceApplication.java_". Supprimez par exemple le " ; " situé à la fin de la lignepackage `org.springframework.samples.petclinic.customers` et commitez les changements dans Git :

[source,bash,linenums]
----
git add spring-petclinic-customers-service/src/main/java/org/springframework/samples/petclinic/customers/CustomersServiceApplication.java
git commit -m "Refactorisation du code des clients"
git push origin refactor-customers
----

Vous devriez maintenant avoir deux branches visibles sur la page d'accueil du projet.

*Puisque vous avez introduit un bug, le pipeline de build est maintenant en échec :*

.Pipeline en échec
image::images/build-failed.png[width=100%]

En cliquant sur la croix rouge dans la colonne Stage, nous avons le détail de l'erreur (ici, le bug que nous avons introduit) :

.Détail de l'erreur d'exécution du pipeline
image::images/build-failed-detail.png[width=100%]

==== Gérez le fix du bug sur GitLab
Pour enregistrer le bug, nous pouvons créer un nouveau Bug directement en cliquant sur le bouton New Issue. Il faut alors remplir les champs adéquats comme l'assignee, le milestone, les labels (Bug et User Story) ou la due date :

.Création d'une nouvelle issue Bug dans GitLab
image::images/new-bug.png[width=100%]

Une fois l'issue complétée, nous avons tous les détails du bug à corriger et le job en échec est automatiquement récupéré :

.Nouveau bug créé
image::images/new-bug-created.png[width=100%]

Si nous revenons sur le board Development, nous nous apercevons que l'issue créée apparaît dans la colonne Open. Nous pouvons alors la déplacer dans la colonne Doing, car nous allons la corriger.

Pour corriger ce bug automatiquement, nous allons créer une merge request, c'est-à-dire demander à commiter les changements sur notre branche dans la branche principale `master`. Pour ce faire, il faut aller dans le menu Merge Requests et cliquer sur New Merge Request.

Dans la nouvelle page, il faut choisir la branche que nous voulons merger dans la branche principale (ici, la branche `monbug`) et cliquer sur "Compare branches and continue" :

.Comparaison des branches Master et Monbug
image::images/compare-branches.png[width=100%]

Au prochain écran, il faut remplir les champs de façon adéquate, et cliquer sur Submit Merge Request. Pour le champ Description, la syntaxe est "Closes" et le numéro du bug, ici, le 6 :

* Title : WIP: Mon premier bug ;
* Description : Closes #6 ;
* Assignee : Assign to me ;
* Milestone : Sprint 1.

.Description de mon bug
image::images/submit-merge-request.png[width=100%]

Une fois la merge request créée, nous avons tous les détails de celle-ci :

.Description de mon bug
image::images/merge-request-detail.png[width=100%]

==== Corrigez votre bug
Il est temps de corriger notre bug. Nous allons éditer le fichier que nous avons modifié. Pour cela, nous allons réintroduire le " ; " manquant, puis commiter le code corrigé sur la branche `monbug`.

[source,bash,linenums]
----
git add spring-petclinic-customers-service/src/main/java/org/springframework/samples/petclinic/customers/CustomersServiceApplication.java
git commit -m "Correction du bug clients"
git push origin monbug
----

Vous allez constater qu'une fois le code pushé sur la branche `monbug`, le pipeline de build se lance afin de vérifier que le code que nous avons envoyé fonctionne.

Une fois que le pipeline s'est terminé en succès, nous pouvons alors enlever le statut Work in progress en cliquant sur le bouton Resolve WIP status, pour ensuite cliquer sur le bouton Merge :

.Bug résolu, mergez les branches !
image::images/merge-branches.png[width=100%]

Le pipeline se lance alors une dernière fois pour vérifier que le code mergé ne casse pas la compilation.

Enfin, si nous revenons sur le board Development, nous voyons que le bug est automatiquement fermé, suite à notre merge request.

=== En résumé

Dans ce chapitre, nous avons implémenté les premières étapes de notre pipeline d'intégration continue grâce à GitLab. 

.Pour cela, nous avons utilisé le fichier gitlab-ci.yml que nous avons configuré pour qu'il lance :
* une étape de build de l'application avec Maven ;
* une étape de test de l'application, en lançant les tests développés précédemment par les développeurs.

.Les 2 premières étapes du pipeline sont fonctionnelles ✅
image::images/ci-etape-build-test.png[width=100%]

[NOTE]
====
.Nous avons donc rempli les 3 premières étapes de l'intégration continue :
. ✅Planifiez votre développement.
. ✅Compilez et intégrez votre code.
. ✅Testez votre code.
. Mesurez la qualité de votre code.
. Gérez les livrables de votre application.
====

Dans le prochain chapitre, nous passerons aux 2 dernières étapes de l'intégration continue.n
