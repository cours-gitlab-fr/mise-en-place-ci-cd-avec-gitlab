<<<
== Garantissez la qualité de votre code
Votre pipeline contient désormais les étapes de build et de test.

.Les étapes de build et de test sont implémentées
image::images/ci-etape-build-test.png[width=100%]

*Dans ce dernier chapitre de la partie CI*, nous allons voir les deux dernières étapes du pipeline d'intégration continue : la *qualité de code* et la *gestion des livrables*.

=== Mesurez la qualité de votre code

Commençons par l'analyse de code statique, afin de contrôler la qualité du code.

Nous allons donc modifier le pipeline de code, afin d'ajouter cette analyse de code. Il faut alors modifier le fichier `.gitlab-ci.yml` afin qu'il ressemble à ceci :

[source,yaml,linenums]
----
stages:
  - build
  - test
  - quality

cache:
  paths:
    - .m2/repository
  key: "$CI_JOB_NAME"

build_job:
  stage: build
  script:
    - ./mvnw compile
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
      --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true
  image: openjdk:8-alpine

test_job:
  stage: test
  script:
    - ./mvnw test
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
      --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true
  image: openjdk:8-alpine

code_quality_job:
  stage: quality
  image: docker:stable
  allow_failure: true
  services:
    - docker:stable-dind
  script:
    - mkdir codequality-results
    - docker run
        --env CODECLIMATE_CODE="$PWD"
        --volume "$PWD":/code
        --volume /var/run/docker.sock:/var/run/docker.sock
        --volume /tmp/cc:/tmp/cc
        codeclimate/codeclimate analyze -f html > ./codequality-results/index.html
  artifacts:
    paths:
      - codequality-results/
----

NOTE: Ici, j'ai ajouté une étape supplémentaire de qualité de code. L'étape (le stage) `quality` est défini dans le bloc `stages`. Cela veut dire que le pipeline va ajouter un job de qualité à la suite de la compilation et des tests. Ce job, je l'ajoute à la fin du fichier et il est appelé `code_quality_job`.

.Dans ce job, nous retrouvons les 3 lignes des étapes précédentes, plus d'autres lignes :
* `allow_failure` : cette ligne *autorise l'échec de l'étape de qualité*. Comme ce n'est pas une étape critique, nous nous permettons d'autoriser l'échec et de laisser le pipeline continuer ;
* `services` :  ce nouveau mot clé de GitLab permet de *démarrer un démon Docker*, pour que l’exécution de notre programme d’analyse de code puisse se faire. Cette ligne nous permet d'exécuter Docker au sein de l'image, afin d'exécuter l'analyse de code ;
* `script` : cette ligne est un peu plus compliquée que les lignes de script précédentes. La première ligne va créer un dossier `codequality-results/` qui contiendra le résultat de l'analyse de code. La deuxième ligne monte le code à l'intérieur d'une image Docker via le dossier `/code`, et lance l'analyse via le programme `codequality`. Le résultat sera exporté dans le dossier `codequality-results` ;
* `artifact` : cette ligne est un prérequis de GitLab si nous voulons voir notre évolution de qualité. Le dossier `codequality-results/` sera stocké au sein de GitLab afin de pouvoir voir le résultat de l'analyse du scan. Ce résultat sera disponible et visible au sein du job `code_quality_job`.

Tout le script est exécuté au sein de l'image `docker:stable`. Cette image permet de démarrer le programme d’analyse de code.

Une fois le fichier commité sous Git et envoyé sur GitLab via les commandes suivantes, le pipeline d'intégration continue va alors se mettre à jour et lancer une compilation, suivie d'une analyse statique du code.

Le code est alors *analysé par GitLab*, et le rapport généré stocké au niveau des artefacts. Pour voir le résultat de l'analyse de code, il suffit de naviguer dans le job `code_quality_job`, puis de cliquer sur Browse. Vous aurez alors accès au dossier contenant le résultat de l'analyse de code, et pourrez naviguer au sein de ce fichier, afin de voir les améliorations à apporter au code.

Ça y est, l'étape de qualité est implémentée !

.L'étape de qualité est implémentée ✅
image::images/ci-etape-build-test-quality.png[width=100%]

NOTE: D'autres outils existent pour voir la qualité d'un code de développement. Le plus connu est SonarQube, qui permet d'afficher des rapports de qualité, l'évolution de ceux-ci, ainsi qu'une détection partielle des erreurs.

=== Packagez votre application pour la déployer

La prochaine étape après la qualité du code est le packaging de l'application, afin de pouvoir la déployer plus facilement. Pour ce projet, nous allons choisir Docker comme programme de packaging.

NOTE: Si vous n'êtes pas à l'aise avec les conteneurs Docker, ils seront expliqués plus en détail *dans la seconde partie de ce cours*, quand nous verrons la livraison continue.

GitLab vient avec une registry Docker incluse, ce qui nous permet de stocker ces images au sein de GitLab. Pour pouvoir packager nos images Docker, il est nécessaire d'ajouter une nouvelle étape à notre pipeline d'intégration continue. Nous allons une nouvelle fois modifier le fichier `.gitlab-ci.yml` pour ajouter cette nouvelle étape. Le fichier final ressemblera alors à ceci :

[source,yaml,linenums]
----
stages:
  - build
  - test
  - quality
  - package

cache:
  paths:
    - .m2/repository
  key: "$CI_JOB_NAME"

build_job:
  stage: build
  script:
    - ./mvnw compile
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
      --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true
  image: openjdk:8-alpine

test_job:
  stage: test
  script:
    - ./mvnw test
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
      --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true
  image: openjdk:8-alpine

code_quality_job:
  stage: quality
  image: docker:stable
  allow_failure: true
  services:
    - docker:stable-dind
  script:
    - mkdir codequality-results
    - docker run
        --env CODECLIMATE_CODE="$PWD"
        --volume "$PWD":/code
        --volume /var/run/docker.sock:/var/run/docker.sock
        --volume /tmp/cc:/tmp/cc
        codeclimate/codeclimate analyze -f html > ./codequality-results/index.html
  artifacts:
    paths:
      - codequality-results/

package_job:
  stage: package
  services:
    - docker:stable-dind
  variables:
    DOCKER_HOST: tcp://docker:2375
  script:
    - apk add --no-cache docker
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - ./mvnw install -PbuildDocker -DskipTests=true -DpushImage
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
      --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true
  image: openjdk:8-alpine
----

NOTE: J'ajoute dans le fichier la dernière étape de mon pipeline d'intégration continue, l'étape (le stage) `package`, qui s'exécutera à la suite de l'étape `quality`. J'ajoute ensuite le job `package_job` associé à cette étape de qualité.

Ce job supplémentaire compile le projet et l'*encapsule dans un conteneur*. Il est ensuite poussé sur la registry de GitLab. Nous retrouvons toutes les lignes que nous avons vues précédemment. La partie `script` lance cependant quelques commandes supplémentaires :

* tout d'abord, nous *installons le client Docker* dans l'image `openjdk:8-alpine` afin de pouvoir lancer les commandes propres à Docker ;
* ensuite, nous *nous connectons sur la registry interne de GitLab* afin de pouvoir pousser les images Docker de façon sécurisée ;
* enfin, *nous lançons la commande Maven* de création de l'image Docker.

Ce processus nous permettra, dans la livraison continue, de pouvoir *déployer facilement* le même code sur différents environnements. Il sert aussi à figer le code compilé dans un *package immuable*. De ce fait, nous pouvons facilement redéployer le même code compilé sur n'importe quel autre environnement. Cela assure que le code ne soit pas modifié entre deux environnements, et qu'un code testé soit *déployé partout de la même façon*. Les images Docker ainsi packagées se retrouvent sur la page de la registry :

.Registry GitLab de votre projet, avec les packages du pipeline
image::images/registry.png[width=100%]

Nous avons maintenant toutes les étapes nécessaires pour l'intégration continue. Comme prévu, notre code est compilé en continu, testé, analysé puis packagé, prêt à être déployé sur de nouveaux environnements.

.Toutes les étapes de l'intégration continue sont implémentées ✅
image::images/ci-complet.png[width=100%]

=== Les autres outils de l'intégration continue
Vous avez donc vu tout au long de cette partie comment mettre en place *l'intégration continue avec GitLab*, mais sachez qu'il existe d'autres outils reprenant les mêmes concepts. Le plus connu et le plus utilisé d'entre eux est Jenkins. Avec cet outil, vous pouvez implémenter toutes les étapes précédemment vues. De plus, Jenkins utilise maintenant un fichier de description comme GitLab, qui s'appelle Jenkinsfile.

Le principe est strictement le même que *GitLab* : il s'agit d'un *fichier de description du pipeline* d'intégration continue qui va contenir toutes les étapes à lancer, afin de garantir que le code compile et soit de qualité à tout moment. Cependant, il est nécessaire d'installer Jenkins dans votre entreprise, de le configurer, de le maintenir et de le mettre à jour, ce qui peut s'avérer *long et fastidieux*.

Pour ceux qui ne voudraient pas passer leur temps à maintenir ce genre d'outils, il existe aussi d'autres outils en mode *Software-as-a-Service* (SaaS), où la maintenance et l'évolution sont garanties par le fournisseur. Ces outils peuvent être mieux adaptés pour mettre en place rapidement et sans effort un pipeline d'intégration continue. Les outils les plus connus dans cet écosystème sont Travis CI et CircleCI.

Le gros avantage de ces outils est que la *maintenance* n'est pas à la charge de l'équipe, mais du *fournisseur*. De plus, ces outils peuvent se connecter automatiquement sur Github.com pour la plupart, ce qui évite aussi les configurations longues et fastidieuses des différents outils.

Enfin, GitHub a sorti une beta de son nouveau service à destination des développeurs, afin de pouvoir implémenter rapidement des pipelines d'intégration continue : *GitHub Actions*. Le principe est toujours le même : un fichier `.workflow` permet de créer un pipeline, afin de compiler et déployer du code sur n'importe quelle plateforme. L'avantage principal de GitHub Actions est que ce dernier est directement intégré dans GitHub.

=== En résumé
Dans ce chapitre, nous avons vu comment ajouter les étapes de mesure de la qualité de votre code ainsi que de packaging de votre application, pour sauvegarder les artefacts prêts à être déployés.

.Nous avons donc rempli les deux dernières étapes de l'intégration continue :
. ✅Planifiez votre développement.
. ✅Compilez et intégrez votre code.
. ✅Testez votre code.
. ✅Mesurez la qualité de votre code.
. ✅Gérez les livrables de votre application.

Et voilà ! Vous avez mis en place toutes les étapes de l'intégration continue ! 👏🎉

Dans la prochaine partie, nous mettrons en place la livraison continue, du déploiement à la supervision, en passant par les tests en production.
